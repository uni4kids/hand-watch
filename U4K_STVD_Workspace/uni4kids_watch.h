#ifndef __UNI4KIDS_WATCH_H
#define __UNI4KIDS_WATCH_H

void BTN1_callback(void);
void BTN2_callback(void);
void RTC_callback(void);
void TimerMs_callback(void);
void BTN1_event(void);
void BTN2_event(void);
void info(void);
void clock(void);
void setmode(void);
void confirmTime(void);

#endif