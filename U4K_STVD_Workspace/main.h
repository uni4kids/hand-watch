#ifndef __MAIH_H
#define __MAIH_H

#define BTN1_PORT           GPIOB
#define BTN1_PIN            GPIO_Pin_1
#define BTN1_EXTI_PIN       EXTI_Pin_1
#define BTN1_EXTI_IT_PIN    EXTI_IT_Pin1

#define BTN2_PORT           GPIOE
#define BTN2_PIN            GPIO_Pin_3
#define BTN2_EXTI_PIN       EXTI_Pin_3
#define BTN2_EXTI_IT_PIN    EXTI_IT_Pin3


#define LSE_STABILISATION_TIME	1000	// ms
#define DEBOUNCE_STABILISATION_TIME	20	// ms
#define TOGGLE_CURSOR_TIME	500	// ms
#define NO_EVENTS_TIMEOUT	10000	// ms

void CLK_init(void);
void TimerMs_init(void);
void GPIO_init(void);
void RTC_init(void);
void I2C_init(void);

#endif
