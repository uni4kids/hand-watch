#include "stm8l15x.h"
#include "main.h"
#include "uni4kids_watch.h"

#include "oled_ssd1306.h"
#include "gfx_bw.h"

#include <stdio.h>
#include <string.h>

extern volatile uint8_t state_update, btn_state, show_cursor_pos;
extern volatile uint8_t edit_cursor_pos;
extern volatile uint8_t btn1_req_active, btn2_req_active;
extern volatile uint8_t btn1_req_time_elapsed, btn2_req_time_elapsed;
extern volatile uint16_t toggle_cursor_time_elapsed;
extern volatile uint16_t no_events_time_elapsed;
extern volatile RTC_TimeTypeDef RTC_curr_time;

// Upper BTN
void BTN1_callback(void)
{
	if (btn1_req_active == 0) 
  {
		btn1_req_active = 1;
		btn1_req_time_elapsed = 0;
	}
}

void BTN1_event(void)
{

  switch(btn_state)
  {
    case 1:
      edit_cursor_pos = 0;
      
      btn_state = 1;
      return;
    case 2:
      edit_cursor_pos = 0;
      
      btn_state = 1;
      break;
    case 3:
      // Move edit cursor with one position left
      // We have 3 positions for hours and minutes
      //  2 10
      // HH:MM:SS
      edit_cursor_pos += 1;
      edit_cursor_pos %= 3;
      break;
    case 4:
      // Apply new time
      RTC_SetTime(RTC_Format_BIN, &RTC_curr_time);
      edit_cursor_pos = 0;
      
      btn_state = 1;
      break;
    default:
      btn_state = 1;
      break;
  }

  state_update = 1;
}

// Lower BTN
void BTN2_callback(void)
{
	if (btn2_req_active == 0) 
  {
    btn2_req_active = 1;
		btn2_req_time_elapsed = 0;
	}
}

void BTN2_event(void)
{
  uint16_t newTimeValue;
  uint8_t oldTimeValue;

  switch(btn_state)
  {
    case 1:
    case 2:
      if(GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == RESET)
      {
        btn_state = 3;
        
        RTC_GetTime(RTC_Format_BIN, &RTC_curr_time);
        RTC_curr_time.RTC_Seconds = 0;
      }
      else
      {
        btn_state = 2;
      }
      break;
    case 3:
      if(GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == RESET)
      {
        btn_state = 4;
      }
      else
      {
        // Increase digit at position edit_cursor_pos
        switch(edit_cursor_pos)
        {
          case 0:
            newTimeValue = RTC_curr_time.RTC_Minutes;
            newTimeValue %= 10; // Take digit position
            newTimeValue += 1;  // Increase the digit
            newTimeValue %= 10; // Fix if overlaped
            
            oldTimeValue = RTC_curr_time.RTC_Minutes;
            oldTimeValue /= 10;
            oldTimeValue *= 10;
            
            newTimeValue = oldTimeValue + newTimeValue;
            RTC_curr_time.RTC_Minutes = (uint8_t)newTimeValue;
            break;
          case 1:
            newTimeValue = RTC_curr_time.RTC_Minutes;
            newTimeValue /= 10; // Take digit position
            newTimeValue += 1;  // Increase the digit
            newTimeValue %= 6;  // Fix if overlaped
            newTimeValue *= 10;
            
            oldTimeValue = RTC_curr_time.RTC_Minutes;
            oldTimeValue %= 10;
            
            newTimeValue = newTimeValue + oldTimeValue;
            RTC_curr_time.RTC_Minutes = (uint8_t)newTimeValue;
            break;
          case 2:
            newTimeValue = RTC_curr_time.RTC_Hours;
            newTimeValue += 1;  // Increase the digit
            newTimeValue %= 24; // Fix if overlaped
            
            RTC_curr_time.RTC_Hours = (uint8_t)newTimeValue;
            break;
          default:
            break;
        }
      }
      break;
    case 4:
      if(GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == RESET)
      {
        btn_state = 4;
      }
      else
      {
        btn_state = 1;
      }
      break;
    default:
      btn_state = 1;
      break;
  }
    
	state_update = 1;
}

void RTC_callback(void)
{
  if (btn_state == 1)
  {
    state_update = 1;
  }
}

void TimerMs_callback(void)
{
	// Button 1 debounce
	if (btn1_req_active == 1) 
	{
		if (btn1_req_time_elapsed > DEBOUNCE_STABILISATION_TIME) 
		{
			if(GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == RESET)
			{
				BTN1_event();
			}
			btn1_req_active = 0;
			btn1_req_time_elapsed = 0;
      no_events_time_elapsed = 0;
		}
		else
		{
			btn1_req_time_elapsed += 1;
		}
	}
	
	// Button 2 debounce
	if (btn2_req_active == 1) 
	{
		if (btn2_req_time_elapsed > DEBOUNCE_STABILISATION_TIME) 
		{
			if(GPIO_ReadInputDataBit(BTN2_PORT, BTN2_PIN) == RESET)
			{
				BTN2_event();
			}
			btn2_req_active = 0;
			btn2_req_time_elapsed = 0;
      no_events_time_elapsed = 0;
		}
		else
		{
			btn2_req_time_elapsed += 1;
		}
	}
  
  if (btn_state == 3)
  {
    if (toggle_cursor_time_elapsed > TOGGLE_CURSOR_TIME)
    {
      if (show_cursor_pos == 0)
      {
        show_cursor_pos = 1;
      }
      else
      {
        show_cursor_pos = 0;
      }
      toggle_cursor_time_elapsed = 0;
      state_update = 1;
    }
    else
    {
      toggle_cursor_time_elapsed += 1;
    }
  }
  else 
  {
    show_cursor_pos = 0;
    toggle_cursor_time_elapsed = 0;
  }

  if (no_events_time_elapsed > NO_EVENTS_TIMEOUT)
  {
    if (btn_state != 1)
    {
      btn_state = 1;
      state_update = 1;
    }
    
    no_events_time_elapsed = 0;
  }
  else
  {
    no_events_time_elapsed +=1;
  }
}

void setmode(void)
{	  
  uint16_t setSec = 0, setMin = 0, setHour = 0;
  char textBuf[15] = {0};
	
	setHour = RTC_curr_time.RTC_Hours;
	setMin = RTC_curr_time.RTC_Minutes;
	setSec = 0;

	SSD1306_Clear(BLACK);
	GFX_SetFontSize(1);
  GFX_DrawString(30, 5, "Time Setup", WHITE, BLACK);
	GFX_DrawLine(10, 16, 118, 16, WHITE);
  if (show_cursor_pos == 0)
  {
    sprintf(textBuf,"%02hu:%02hu:%02hu", setHour, setMin, setSec);
  }
  else 
  {
    switch(edit_cursor_pos)
    {
      case 0:
        setMin /= 10;
        sprintf(textBuf,"%02hu:%1u_:%02hu", setHour, setMin, setSec);
        break;
      case 1:
        setMin %= 10;
        sprintf(textBuf,"%02hu:_%1u:%02hu", setHour, setMin, setSec);
        break;
      case 2:
        sprintf(textBuf,"__:%02hu:%02hu", setMin, setSec);
        break;
      default:
        sprintf(textBuf,"%02hu:%1u_:%02hu", setHour, setMin, setSec);
        break;
    }
  }
	
 	GFX_DrawString(37, 20, textBuf, WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}

void confirmTime(void)
{	
  uint16_t setSec = 0, setMin = 0, setHour = 0;
  char textBuf[15] = {0};

	setHour = RTC_curr_time.RTC_Hours;
	setMin = RTC_curr_time.RTC_Minutes;
	setSec = 0;

  SSD1306_Clear(BLACK);
  GFX_SetFontSize(1);
  GFX_DrawString(3, 5, "Yes", WHITE, BLACK);
  GFX_DrawString(42, 5, "Apply Time", WHITE, BLACK);
	GFX_DrawLine(1, 16, 128, 16, WHITE);
  GFX_DrawString(5, 20, "No", WHITE, BLACK);
  sprintf(textBuf,"%02hu:%02hu:%02hu", setHour, setMin, setSec);
  GFX_DrawString(50, 20, textBuf, WHITE, BLACK);
  GFX_DrawLine(25, 0, 25, 32, WHITE);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}

void clock(void)
{
	static char buf[15] = {0};
	static uint16_t sec = 0, min = 0, hour = 0;
	static RTC_TimeTypeDef RTC_time;
	RTC_GetTime(RTC_Format_BIN, &RTC_time);
	
	hour = RTC_time.RTC_Hours;
	min = RTC_time.RTC_Minutes;
	sec = RTC_time.RTC_Seconds;
	sprintf(buf,"%02hu:%02hu:%02hu", hour, min, sec);
	
	SSD1306_Clear(BLACK);
	GFX_SetFontSize(2);
	GFX_DrawString(14, 9, buf, WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}

void info(void)
{
	SSD1306_Clear(BLACK);
	GFX_SetFontSize(1);
	GFX_DrawString(20, 5, "Uni4Kids 2021", WHITE, BLACK);
	GFX_DrawLine(10, 16, 118, 16, WHITE);
	GFX_DrawString(30, 20, "Vankata513", WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}