#include "stm8l15x.h"
#include "stm8l15x_tim3.h"
#include "main.h"
#include "uni4kids_watch.h"
#include "timing_delay.h"

#include "oled_hw_setup.h"
#include "oled_ssd1306.h"
#include "gfx_bw.h"
#include "fonts.h"

#define TIM3_PERIOD  (uint8_t) 7

volatile uint8_t state_update = 0, btn_state = 1, show_cursor_pos = 0;
volatile uint8_t edit_cursor_pos = 0;
volatile uint8_t btn1_req_active = 0, btn2_req_active = 0;
volatile uint8_t btn1_req_time_elapsed = 0, btn2_req_time_elapsed = 0;
volatile uint16_t toggle_cursor_time_elapsed = 0;
volatile uint16_t no_events_time_elapsed = 0;
volatile RTC_TimeTypeDef RTC_curr_time;

void main(void)
{
	CLK_init();
	GPIO_init();
	TimingDelay_Init();
	TimerMs_init();
	enableInterrupts();
	Delay(LSE_STABILISATION_TIME);
	RTC_init();
	I2C_Config();
	SSD1306_Init();
	SSD1306_SetContrast(0x50);
	GFX_SetFont(font_7x5);
	GFX_SetFontSize(2);
	
	//GPIO_Init(GPIOB, GPIO_Pin_0, GPIO_Mode_Out_PP_Low_Slow);
	//GPIO_SetBits(GPIOB, GPIO_Pin_0);
  //GPIO_ResetBits(GPIOB, GPIO_Pin_0);
	
	while(1)
	{
		if(state_update == 1)
		{
      state_update = 0;
			switch(btn_state)
			{
				case 1:
					clock();
					break;
				case 2:
					info();
					break;
				case 3:
					setmode();
					break;
        case 4:
					confirmTime();
					break;
				default:
					clock();
					break;
			}
		}
	}
}

void CLK_init(void)
{
	CLK_DeInit();

	// Select HSI as system clock source. This automatically turns it on.
	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
	// Wait for HSI to start
	while (CLK_GetFlagStatus(CLK_FLAG_HSIRDY) != SET);
	// Perform the system clock switch
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
	// Set the system clock prescaler to 16, resulting in 1 MHz system clock
	CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_16);

	// Turn on the LSE
	CLK_LSEConfig(CLK_LSE_ON);
	// Wait for LSE to start
	while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) != SET);
	// Enable the RTC peripheral clock (gated instance of the system clock)
	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
	// Set LSE with no prescaling as the RTC clock source
	CLK_RTCClockConfig(CLK_RTCCLKSource_LSE, CLK_RTCCLKDiv_1);

	// TODO: CSS

	// Disable unused clock sources
	CLK_LSICmd(DISABLE);
	CLK_HSEConfig(CLK_HSE_OFF);
		
	// Turn on CCO with LSE/1 clock on pin PC4
	//CLK_CCOConfig(CLK_CCOSource_LSE, CLK_CCODiv_1);
	CLK_CCOConfig(CLK_CCOSource_Off, CLK_CCODiv_1);

	// Enable peripheral clock for the OLED
	#ifdef SSD1306_SPI_CONTROL
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);
	#else
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, DISABLE);
	#endif
	#ifdef SSD1306_I2C_CONTROL
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
	#else
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, DISABLE);
	#endif

	// Disable BOOT ROM peripheral clock
	CLK_PeripheralClockConfig(CLK_Peripheral_BOOTROM, DISABLE);
}

void GPIO_init(void)
{
	// Turn on CCO with LSE/1 clock on pin PC4
	//GPIO_Init(GPIOC, GPIO_Pin_4, GPIO_Mode_Out_PP_Low_Slow);
	
	// Turn on RTC CAL Output on pin PD6
	//GPIO_Init(GPIOD, GPIO_Pin_6, GPIO_Mode_Out_PP_Low_Slow);
	
	// Setup Button inputs and external interrupt enable
	EXTI_DeInit();
	GPIO_Init(BTN1_PORT, BTN1_PIN, GPIO_Mode_In_PU_IT);
	GPIO_Init(BTN2_PORT, BTN2_PIN, GPIO_Mode_In_PU_IT);
	EXTI_SetPinSensitivity(BTN1_EXTI_PIN, EXTI_Trigger_Falling);
	EXTI_SetPinSensitivity(BTN2_EXTI_PIN, EXTI_Trigger_Falling);
	EXTI_ClearITPendingBit(BTN1_EXTI_IT_PIN);
	EXTI_ClearITPendingBit(BTN2_EXTI_IT_PIN);
	
	// OLED I2C output settings
	OLED_GPIO_Config();
}

void RTC_init(void)
{
	RTC_InitTypeDef RTC_init_params;
	RTC_TimeTypeDef RTC_time;
	RTC_DateTypeDef RTC_date;

	// Assumption: Clock control has already been configured and RTCCLK is 32.768 kHz
	RTC_init_params.RTC_HourFormat = RTC_HourFormat_24;
	RTC_init_params.RTC_AsynchPrediv = 0x7F;
	RTC_init_params.RTC_SynchPrediv = 0x00FF;
	RTC_Init(&RTC_init_params);

	RTC_DateStructInit(&RTC_date);
	RTC_date.RTC_WeekDay = RTC_Weekday_Saturday;
	RTC_date.RTC_Date = 07;
	RTC_date.RTC_Month = RTC_Month_August;
	RTC_date.RTC_Year = 21;
	RTC_SetDate(RTC_Format_BIN, &RTC_date);

	RTC_TimeStructInit(&RTC_time);
	RTC_time.RTC_Hours   = 23;
	RTC_time.RTC_Minutes = 22;
	RTC_time.RTC_Seconds = 40;
	RTC_SetTime(RTC_Format_BIN, &RTC_time);
	
	// Turn on RTC CAL Output on pin PD6
	//RTC_CalibOutputConfig(RTC_CalibOutput_512Hz);
	//RTC_CalibOutputCmd(ENABLE);
		
	// Configures the RTC wakeup timer_step = RTCCLK/16 = LSE/16 = 488.28125 us 
  RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
	//RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);

  // Enable wake up unit Interrupt
  RTC_ITConfig(RTC_IT_WUT, ENABLE);

  // RTC wake-up event every 1000 ms (timer_step x (2047 + 1) )
  RTC_SetWakeUpCounter(2047);
  RTC_WakeUpCmd(ENABLE);
}

void TimerMs_init(void)
{
 /* Enable TIM3 clock */
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);

  /* Remap TIM3 ETR to LSE: TIM2 external trigger becomes controlled by LSE clock */
  SYSCFG_REMAPPinConfig(REMAP_Pin_TIM3TRIGLSE, ENABLE);

  /* Enable LSE clock */
  CLK_LSEConfig(CLK_LSE_ON);
  /* Wait for LSERDY flag to be reset */
  while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) == RESET);

  /* TIM3 configuration:
     - TIM3 ETR is mapped to LSE
     - TIM3 counter is clocked by LSE div 4
      so the TIM2 counter clock used is LSE / 4 = 32.768 / 4 = 8.192 KHz
     TIM3 Channel1 output frequency = TIM3CLK / (TIM3 Prescaler * (TIM3_PERIOD + 1))
                                    = 8192 / (1 * 8) = 1024 Hz                */
                                    
  /* Time Base configuration */
  TIM3_TimeBaseInit(TIM3_Prescaler_1, TIM3_CounterMode_Up, TIM3_PERIOD);
  TIM3_ETRClockMode2Config(TIM3_ExtTRGPSC_DIV4, TIM3_ExtTRGPolarity_NonInverted, 0);

  TIM3_UpdateRequestConfig(TIM3_UpdateSource_Global);

  /* Clear TIM3 update flag */
  TIM3_ClearFlag(TIM3_FLAG_Update);

  /* Enable update interrupt */
  TIM3_ITConfig(TIM3_IT_Update, ENABLE);

  TIM3_Cmd(ENABLE);
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	(void) file;
	(void) line;
	/* Infinite loop */
	while (1);
}

#endif
