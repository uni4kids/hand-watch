// [(0xFFFF - 0x8000) - (0xFFFF - 0xB4F5)] bytes = 13.557kB

// Halt for 1s
    /*
    // RTC wake-up event every 1000 ms (timer_step x (2047 + 1) )
    RTC_SetWakeUpCounter(2047);
    RTC_WakeUpCmd(ENABLE);
    
    // Enter Halt mode
    halt();

    RTC_WakeUpCmd(DISABLE);
    */

void GFX_DrawChar(int x, int y, char chr, uint8_t color, uint8_t background)
{
	uint8_t i = 0, j = 0;
	
  if(chr > 0x7E) return; /* chr > '~' */

  for(i = 0; i < font[1]; i++) {
    uint8_t line = (uint8_t)font[(chr - 0x20) * font[1] + i + 2];

    for(j = 0; j < (uint8_t)font[0]; j++, line >>= 1) {
      if(line & 1) {
        GFX_DrawPixel(x + i, y + j, color);
      }
      else
        if(background == 0) {
          GFX_DrawPixel(x + i, y + j, background);
      }
    }
  }
}


0 0 1 1 2 2 3 3 4 4  5  5  6  6  7  7

0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15



const uint8_t font_7x7[] =
{
  8, 8, /* height, width */
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x5F, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x07, 0x00, 0x00, 0x07, 0x00, 0x00,
  0x14, 0x14, 0x7F, 0x14, 0x7F, 0x14, 0x14, 0x00, // #
  0x24, 0x2A, 0x2A, 0x7F, 0x2A, 0x2A, 0x12, 0x00,
  0x00, 0x4E, 0x2A, 0x1E, 0x78, 0x54, 0x72, 0x00,
  0x30, 0x4A, 0x45, 0x45, 0x4D, 0x32, 0x50, 0x00,
  0x00, 0x00, 0x05, 0x03, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x1C, 0x22, 0x41, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x41, 0x22, 0x1C, 0x00, 0x00, 0x00,
  0x00, 0x08, 0x2A, 0x1C, 0x08, 0x1C, 0x2A, 0x08 // ...
};

0x14, 0x14, 0x7F, 0x14, 0x7F, 0x14, 0x14, 0x00

0x14 = 0001 0100
0x7F = 0111 1111

00000000
00101000
00101000
11111110
00101000
11111110
00101000
00101000

0000000000000000
0000110011000000
0000110011000000
1111111111111100
0000110011000000
1111111111111100
0000110011000000
0000110011000000

0000000000000000
0000000000000000
0000110011000000
0000110011000000
0000110011000000
0000110011000000
1111111111111100
1111111111111100
0000110011000000
0000110011000000
1111111111111100
1111111111111100
0000110011000000
0000110011000000
0000110011000000
0000110011000000

void GFX_DrawChar(int x, int y, char chr, uint8_t color, uint8_t background)
{
  uint8_t i = 0, j = 0;
  
  if(chr > 0x7E) return; /* chr > '~' */

  for(i = 0; i < font[1]; i++) {
    uint8_t line = (uint8_t)font[(chr - 0x20) * font[1] + i + 2];

    for(j = 0; j < (uint8_t)font[0]; j++, line >>= 1) {
      if(line & 1) {
        if(size == 1)
          GFX_DrawPixel(x + i, y + j, color);
        else
          GFX_DrawFillRectangle(x + i * size, y + j * size, size, size, color);
      }
      else
        if(background == 0) {
          if(size == 1)
            GFX_DrawPixel(x + i, y + j, background);
          else
            GFX_DrawFillRectangle(x + i * size, y + j * size, size, size, background);
      }
    }
  }
}






void GFX_DrawChar(int x, int y, char chr, uint8_t color, uint8_t background)
{
  uint8_t i = 0, j = 0;
  
  if(chr > 0x7E) return; /* chr > '~' */

  for(i = 0; i < font[1]; i++) {
    uint8_t line = (uint8_t)font[(chr - 0x20) * font[1] + i + 2];

    for(j = 0; j < (uint8_t)font[0]; j++, line >>= 1) {
      if(line & 1) {
#if (USE_STRETCH_FONTS == 1)
        if(size == 1)
          GFX_DrawPixel(x + i, y + j, color);
        else
        GFX_DrawFillRectangle(x + i * size, y + j * size, size, size, color);
#else
        GFX_DrawPixel(x + i, y + j, color);
#endif
      }
      else
        if(background == 0) {
#if (USE_STRETCH_FONTS == 1)
          if(size == 1)
            GFX_DrawPixel(x + i, y + j, background);
          else
            GFX_DrawFillRectangle(x + i * size, y + j * size, size, size, background);
#else
          GFX_DrawPixel(x + i, y + j, background);
#endif
      }
    }
  }
}



void GFX_DrawFillRectangle(int x, int y, uint16_t w, uint16_t h, uint8_t color)
{
  int i = 0;
    for(i = x; i < x + w; i++) {
      GFX_DrawFastVLine(i, y, h, color);
    }
    
}



  //GFX_DrawString(30, 5, "HH:MM:SS", WHITE, BLACK);
  //GFX_DrawString(30, 18, buf, WHITE, BLACK);