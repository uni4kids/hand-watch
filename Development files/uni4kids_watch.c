#include "main.h"
#include "uni4kids_watch.h"
#include "timing_delay.h"
#include "oled_ssd1306.h"
#include "gfx_bw.h"
#include <stdio.h>
//#include <stdlib.h>
#include <string.h>

//void GFX_WriteLine(int x_start, int y_start, int x_end, int y_end, uint8_t color);
extern volatile uint8_t state_update, btn_state;
char buf[15] = {0};
RTC_TimeTypeDef RTC_time;
volatile uint8_t setting = 0;
volatile uint16_t sec = 0, min = 0, hour = 0;

// Upper BTN
void BTN1_callback(void)
{
	if(setting == 0)
	{
		btn_state = 1;
	}
	else
	{
		if(setting == 1) hour++;
		else if(setting == 2) min++;
		else if(setting == 3) sec++;
	}
	state_update = 1;
	Delay(2);
}

// Lower BTN
void BTN2_callback(void)
{
	if(setting == 0)
	{
		if(GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == RESET)
		{
			btn_state = 3;
			setting = 1;
		}
		else
		{
			btn_state = 2;
		}
	}
	else
	{
		setting++;
	}
	state_update = 1;
	Delay(2);
}

void RTC_callback(void)
{
	state_update = 1;
}

void setmode(void)
{
	if(setting > 3)
	{
		sprintf(buf,"Applied");
		setting = 0;
		btn_state = 1;
		// Set RTC Time
		SSD1306_Clear(BLACK);
		GFX_DrawString(30, 5, "Time Setup", WHITE, BLACK);
		GFX_DrawString(32, 16, buf, WHITE, BLACK);
		SSD1306_Display();
		Delay(1500);
	}
	else
	{
		sprintf(buf,"%2hu:%2hu:%2hu", hour, min, sec);
	}
	
	SSD1306_Clear(BLACK);
	GFX_SetFontSize(1);
	GFX_DrawString(30, 5, "Time Setup", WHITE, BLACK);
	GFX_DrawString(14, 20, buf, WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}

void clock(void)
{
	RTC_GetTime(RTC_Format_BIN, &RTC_time);
	//GPIO_SetBits(GPIOB, GPIO_Pin_0);
	hour = RTC_time.RTC_Hours;
	min = RTC_time.RTC_Minutes;
	sec = RTC_time.RTC_Seconds;
	sprintf(buf,"%2hu:%2hu:%2hu", hour, min, sec);
	//GPIO_ResetBits(GPIOB, GPIO_Pin_0);
	
	SSD1306_Clear(BLACK);
	GFX_SetFontSize(2);
	GFX_DrawString(14, 9, buf, WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}

void test(void)
{
	SSD1306_Clear(BLACK);
	GFX_SetFontSize(1);
	GFX_DrawString(20, 5, "Uni4Kids 2021", WHITE, BLACK);
	GFX_DrawLine(10, 16, 118, 16, WHITE);
	GFX_DrawString(30, 20, "Vankata513", WHITE, BLACK);
	GFX_DrawRectangle(0, 0, 128, 32, WHITE);
	SSD1306_Display();
}