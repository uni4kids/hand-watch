# Hand Watch

A digital hand watch based on STM8L052C6T6 MCU.

## Description
This project is digital watch developed by [Uni4Kids](https://uni4kids.bg/) electronics team. It is based on STM8L052C6T6 MCU, SSD1306 128x32 OLED display and is powered by CR1220 battey cell. PCBs will be available soon in the [Uni4Kids online store](https://things.uni4kids.bg/).

## Visuals
- Add picture of the watch

## Installation
The project can be directly opened in ST Visual Develop IDE. It is build using COSMIC C89 compiler for STM8. The program ca be uploaded directly from the IDE using the *debug* button or upload the binary `*.s19` file using ST Visual Programmer selecting the right MCU(STM8L052C6T6) and the used programmer (ST-Link V2 was used when developing this project).

## Usage
This project is made to be used with the custom Handwatch PCB designed in EasyEDA by Uni4Kids electronics team.

## Roadmap
- Test runtime on battery power
- Try to improve battery runtime if needed and practicaly achievable

## Used libraries
[Uni4Kids OLED SSD1306 STM8L](https://gitlab.com/uni4kids/oled_ssd1306_stm8l/-/tree/main)

## Authors and acknowledgment
[Vladimir Garistov](https://gitlab.com/vl_garistov)     = [Original code](https://gitlab.com/vl_garistov/uni4kids_watch_stm8) author  
[Ivan Stefanov](https://gitlab.com/ivan.stefanov.513)   = Fixed OLED display library and added some functions  
[Dragomir Yanev](https://gitlab.com/dragomir.yanev)     = Added button debounce,settings and various fixes

## License
This project is licensed under MIT license so anyone can use it for free.

## Project status
The HandWatch is almost finished:
- Finished
    * MCU is configured
    * RTC is working
    * Display is working
    * Clock, Info and Time Setup modes are working
    * Time Setup mode is ready
    * Button debounce is implemented
    * Fixed program stability and various bugs
- Work in progress
    * Test and improve battery runtime
